require('dotenv').config({ path: __dirname + '/../.env' });
var express = require('express');
var dbConn = require('./app/config/Db/config');
var bodyParser = require('body-parser');
const cookieParser = require('cookie-parser')
var app = express();
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

var cartRouter = require('./app/routers/Cart');
const cors = require("cors");

app.use(cors(
    {
        allowedHeaders: ['Content-Type', 'Authorization'],
        origin: ["http://localhost:3000", "http://125.235.232.245:3000"],
        credentials: true
    }
));

app.use(function (req, res, next) {
    console.log(req.cookies,req.ip)
    res.header('Content-Type', 'application/json;charset=UTF-8')
    res.header('Access-Control-Allow-Origin', "http://localhost:3000");
    res.header('Access-Control-Allow-Credentials', true)
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept, Set-Cookie'
    )
    next()
})
app.set('port', process.env.PORT || 3004);

app.get('/', function (req, res) {
    return res.send({ error: true, message: 'hello' })
});

app.use('/api/cart', cartRouter);

app.listen(3004, function () {
    console.log('Oder service port 3004');
    setInterval(function () {
        dbConn.query('SELECT version()', function (error, results, fields) {
            if (error) throw error;
            console.log('Oder service port 3004');
        });
    }, 300000);
});

module.exports = app;