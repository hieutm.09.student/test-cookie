const express = require('express');
const router = express.Router();
/**
 * Thêm sản phẩm vào giỏ hàng
 */
router.get('/add/', async (req, res) => {
    if (req.cookies.cart) {
        req.cookies.cart[0].qty++;
        res.cookie('cart', req.cookies.cart);
        return res.send({ status: "Đã có giỏ hàng", data: req.cookies.cart })
    } else {
        res.cookie('cart', [{ product_id: 1, qty: 1 }]);
        return res.send({ status: "Tạo giỏ hàng", data: req.cookies.cart })
    }
});

module.exports = router;